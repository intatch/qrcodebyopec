import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Navigation, ReadQRcode } from './companents/index.js'
import { GenQRcode } from './containers/index.js'


class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <div>
          <Navigation />
          <Switch>
            <Route path="/genqrcode" component={GenQRcode} exact />
            <Route path="/readqrcode" component={ReadQRcode} />
          </Switch>

          </div>
        </Router>
      </div>
    );
  }
}

export default App;
