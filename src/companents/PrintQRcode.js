import React, { Component } from 'react'

class PrintQRcode extends Component {
    render() {
        let { URLQRcodes, size } = this.props
        return (
            <div className="page grid" >
                {
                    URLQRcodes.map((value, index) =>
                        <div className="module" style={{ width: size + "cm" }} key={value.index}>
                            <img style={{ width: size + "cm" }} alt={value.index} src={value.URLdata} />
                            <p style={{ fontSize: size + 9 , textAlign: 'center', margin: 0}}>{value.index}</p>
                        </div>
                    )
                }
            </div>)
    }

}
PrintQRcode.defaultProps = {

}

export default PrintQRcode