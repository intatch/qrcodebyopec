import React, { Component } from 'react'
import QrReader from "react-qr-reader";

export default class ReadQRcode extends Component {
    state = {
        result: "noooooooooo !"
    }
    handleScan = (data) => {
        if (data) {
            alert(data)
            this.setState({
                result: data,
            })
        }
    }
    handleError(err) {
        console.error(err)
    }
    render() {
        return (
            <div>
                <div className="split left">
                        <QrReader
                            delay={this.state.delay}
                            onError={this.handleError}
                            onScan={this.handleScan}
                            style={{ width: '100%', height: '100%' }}
                        />
                </div>

                <div className="split right">
                    <div className="centered">
                        <h1>{this.state.result}</h1>
                    </div>
                </div>


            </div>
        )
    }
}

