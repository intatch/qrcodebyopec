import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
export default class Navigation extends Component {
    render() {
        return (
            <div className="App">
                <NavLink to="/genqrcode">Gan QR code</NavLink>
                <NavLink to="/readqrcode">Read QR code</NavLink>
            </div>
        )
    }
}
